<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:h="HouseChema" xmlns:i="HouseInfo" xmlns:r="RoomsChema" version="1.0">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    <xsl:template match="/">
        <AllRooms>
            <xsl:for-each select="h:Houses">
                <xsl:apply-templates select="h:House">
                    <xsl:sort select="@City" />
                </xsl:apply-templates>
            </xsl:for-each>
        </AllRooms>
    </xsl:template>

    <xsl:template match="h:House">
        <xsl:for-each select="h:Blocks">
            <xsl:apply-templates select="h:Block">
                <xsl:sort select="@number" />
            </xsl:apply-templates>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="h:Block">
        <xsl:param name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
        <xsl:param name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'" />
        <xsl:param name="HouseGuestsCount" select="sum(ancestor::h:House/h:Blocks/h:Block/r:Room/@guests | ancestor::h:House/h:Blocks/h:Block/r:Rooms/r:Room/@guests)"/>
        <xsl:param name="HouseRoomsCount" select="count(ancestor::h:House/h:Blocks/h:Block/r:Room | ancestor::h:House/h:Blocks/h:Block/r:Rooms/r:Room)" />
        <xsl:for-each select="r:Room | r:Rooms/r:Room">
            <xsl:sort select="@nuber" data-type="number"/>
            <Room>
                <Address>
                    <xsl:value-of select="concat(translate(ancestor::h:House/@City, $lowercase, $uppercase), '/', ancestor::h:House/i:Address, '/', ancestor::h:Block/@number, '/', @nuber)" />
                </Address>
                <HouseRoomsCount>
                    <xsl:value-of select="$HouseRoomsCount"/>
                </HouseRoomsCount>
                <BlockRoomsCount>
                    <xsl:value-of select="count(ancestor::h:Block/r:Room | ancestor::h:Block/r:Rooms/r:Room)"/>
                </BlockRoomsCount>
                <HouseGuestsCount>
                    <xsl:value-of select="$HouseGuestsCount"/>
                </HouseGuestsCount>
                <GuestsPerRoomAverage>
                    <xsl:value-of select="floor($HouseGuestsCount div $HouseRoomsCount)"/>
                </GuestsPerRoomAverage>
                    <xsl:choose>
                        <xsl:when test="@guests = 1">
                            <Allocated Single="true" Double="false" Triple="false" Quarter="false" />
                        </xsl:when>
                        <xsl:when test="@guests = 2">
                            <Allocated Single="false" Double="true" Triple="false" Quarter="false" />
                        </xsl:when>
                        <xsl:when test="@guests = 3">
                            <Allocated Single="false" Double="false" Triple="true" Quarter="false" />
                        </xsl:when>
                        <xsl:when test="@guests = 4">
                            <Allocated Single="false" Double="false" Triple="false" Quarter="true" />
                        </xsl:when>
                    </xsl:choose>
            </Room>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>