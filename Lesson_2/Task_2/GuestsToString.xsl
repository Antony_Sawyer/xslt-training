<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:st="xml_version_1.0" 
    xmlns:end="https://www.meme-arsenal.com/create/template/43024" 
    version="1.0">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    <!-- Разделители: '#' для атрибутов, '\' для элементов, '|' для человека. Порядок атрибутов и элементов - в порядке появления в оригинальном файле. -->
    <xsl:template match="/">
        <Guests>
            <xsl:for-each select="st:Guests">
                <xsl:apply-templates select="st:Guest | end:Guest" />
            </xsl:for-each>
        </Guests>
    </xsl:template>
    
    <xsl:template match="st:Guest | end:Guest">
        <xsl:param name="attr" select="'#'" />
        <xsl:param name="tag" select="'/'" />
        <xsl:param name="people" select="'|'" />
        <xsl:value-of select="concat(name(), $attr, @Age, $attr, @Nationalty, $attr, @Gender, $attr, @Name, $tag, st:Type/text(), $tag, st:Profile/st:Address, $tag, st:Profile/st:Email, $people)"/>
    </xsl:template>

</xsl:stylesheet>
