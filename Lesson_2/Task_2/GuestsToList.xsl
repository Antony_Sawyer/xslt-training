<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:st="xml_version_1.0"
    xmlns:end="https://www.meme-arsenal.com/create/template/43024" version="1.0">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    <!-- Разделители: '#' для атрибутов, '\' для элементов, '|' для человека. Порядок атрибутов и элементов - в порядке появления в оригинальном файле. -->
    <xsl:template match="/">
        <Guests>
            <xsl:apply-templates select="Guests"/>
        </Guests>
    </xsl:template>

    <xsl:template name="ToList" match="Guests">
        <xsl:param name="text" select="text()"/>
        <xsl:param name="delimeter" select="'|'"/>
        <xsl:choose>
            <xsl:when test="contains($text, $delimeter)">
                <xsl:element name="{substring-before($text, '#')}">
                    <xsl:attribute name="Age">
                        <xsl:value-of select="substring-before(substring-after($text, '#'), '#')"/>
                    </xsl:attribute>
                    <xsl:attribute name="Nationalty">
                        <xsl:value-of
                            select="substring-before(substring-after(substring-after($text, '#'), '#'), '#')"
                        />
                    </xsl:attribute>
                    <xsl:attribute name="Gender">
                        <xsl:value-of
                            select="substring-before(substring-after(substring-after(substring-after($text, '#'), '#'), '#'), '#')"
                        />
                    </xsl:attribute>
                    <xsl:attribute name="Name">
                        <xsl:value-of
                            select="substring-before(substring-after(substring-after(substring-after(substring-after($text, '#'), '#'), '#'), '#'), '/')"
                        />
                    </xsl:attribute>
                    <Type>
                        <xsl:value-of select="substring-before(substring-after($text, '/'), '/')"/>
                    </Type>
                    <Profile>
                        <Address>
                            <xsl:value-of
                                select="substring-before(substring-after(substring-after($text, '/'), '/'), '/')"
                            />
                        </Address>
                        <xsl:variable name="email"
                            select="substring-before(substring-after(substring-after(substring-after($text, '/'), '/'), '/'), $delimeter)"/>
                        <xsl:if test="$email">
                            <Email>
                                <xsl:value-of select="$email"/>
                            </Email>
                        </xsl:if>
                    </Profile>
                </xsl:element>
                <xsl:call-template name="ToList">
                    <xsl:with-param name="text" select="substring-after($text, $delimeter)"/>
                    <xsl:with-param name="delimeter" select="$delimeter"/>
                </xsl:call-template>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
