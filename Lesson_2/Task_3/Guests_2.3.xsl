<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="xml_version_1.0" xmlns:end="https://www.meme-arsenal.com/create/template/43024" version="1.0">
    <xsl:template match="*">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="@*">
        <xsl:attribute name="{translate(local-name(), 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')}">
            <xsl:value-of select="."/>
        </xsl:attribute>
    </xsl:template>
    <xsl:template match="*[local-name() = 'Guests']">
        <Peoples>
            <xsl:apply-templates select="@* | node()"/>
        </Peoples>
    </xsl:template>
    <xsl:template match="*[local-name() = 'Guest']">
        <People>
            <xsl:apply-templates select="@* | node()"/>
        </People>
    </xsl:template>
    <xsl:template match="text()[normalize-space(.)]">
        <Text>
            <xsl:value-of select="."/>
        </Text>
    </xsl:template>
</xsl:stylesheet>