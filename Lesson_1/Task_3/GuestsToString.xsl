<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<!-- Разделители: '#' для атрибутов, '\' для элементов, '|' для человека. Порядок атрибутов и элементов - в порядке появления в оригинальном файле. -->
    <xsl:template match="/">
            <Guests>
                <xsl:for-each select="*/*">
                    <xsl:call-template name="ToString">
                        <xsl:with-param name="age" select="concat('#', @Age)" />
                        <xsl:with-param name="nationalty" select="concat('#', @Nationalty)" />
                        <xsl:with-param name="gender" select="concat('#', @Gender)" />
                        <xsl:with-param name="name" select="concat('#', @Name)" />
                        <xsl:with-param name="type" select="concat('\', ./*[1]/text())" />
                        <xsl:with-param name="address" select="concat('\', ./*/*[1]/text())"/>
                        <xsl:with-param name="email" select="concat('\', ./*/*[2]/text())"/>
                    </xsl:call-template>
                </xsl:for-each>
            </Guests>        
    </xsl:template>

    <xsl:template name="ToString">
        <xsl:param name="age" />
        <xsl:param name="nationalty" />
        <xsl:param name="gender" />
        <xsl:param name="name" />
        <xsl:param name="type" />
        <xsl:param name="address" />
        <xsl:param name="email" />
            <xsl:value-of select="concat($age, $nationalty, $gender, $name, $type, $address, $email, '|')"/>
    </xsl:template>

</xsl:stylesheet>
