<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    <!-- Разделители: '#' для атрибутов, '\' для элементов, '|' для человека. Порядок атрибутов и элементов - в порядке появления в оригинальном файле. -->
    <xsl:template match="/">
        <Guests>
            <xsl:for-each select="*">
                    <xsl:call-template name="PeopleToList">
                        <xsl:with-param name="text" select="text()"/>
                        <xsl:with-param name="delimeter" select="'|'"/>
                    </xsl:call-template>
            </xsl:for-each>
        </Guests>

    </xsl:template>

    <xsl:template name="PeopleToList">
        <xsl:param name="text"/>
        <xsl:param name="delimeter"/>
        <xsl:choose>
            <xsl:when test="contains($text, $delimeter)">
                <Guest>
                    <xsl:attribute name="Age">
                        <xsl:value-of select="substring-before(substring-after($text, '#'), '#')"/>
                    </xsl:attribute>                    
                    <xsl:attribute name="Nationalty">
                        <xsl:value-of select="substring-before(substring-after(substring-after($text, '#'), '#'), '#')"/>
                    </xsl:attribute>                    
                    <xsl:attribute name="Gender">
                        <xsl:value-of select="substring-before(substring-after(substring-after(substring-after($text, '#'), '#'), '#'), '#')"/>
                    </xsl:attribute>                    
                    <xsl:attribute name="Name">
                        <xsl:value-of select="substring-before(substring-after(substring-after(substring-after(substring-after($text, '#'), '#'), '#'), '#'), '\')"/>
                    </xsl:attribute>
                    <Type>
                        <xsl:value-of select="substring-before(substring-after($text, '\'), '\')"/>
                    </Type>
                    <Profile>
                        <Address>
                            <xsl:value-of select="substring-before(substring-after(substring-after($text, '\'), '\'), '\')"/>
                        </Address>
                        <Email>
                            <xsl:value-of select="substring-before(substring-after(substring-after(substring-after($text, '\'), '\'), '\'), '|')"/>
                        </Email>
                    </Profile>
                </Guest>
                <xsl:call-template name="PeopleToList">
                    <xsl:with-param name="text" select="substring-after($text, $delimeter)"/>
                    <xsl:with-param name="delimeter" select="$delimeter"/>
                </xsl:call-template>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
