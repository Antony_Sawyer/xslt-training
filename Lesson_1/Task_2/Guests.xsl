<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    <xsl:template match="/">
        <result>
            <task1>
                <xsl:for-each select="//*[@Name = 'Jimmy']/preceding-sibling::*">
                    <xsl:call-template name="FirstTask"/>
                </xsl:for-each>
            </task1>
            <task2>
                <!-- Почему-то не работает "Address/text()" вместо последней звезды (для отборки именно адреса), поэтому берём первый элемент из <profile> -->
                <xsl:for-each
                    select="//*[@Name = 'Jimmy']/following-sibling::*[@Nationalty != 'BY']/*/*[1]">
                    <xsl:call-template name="SecondTask"/>
                </xsl:for-each>
            </task2>
            <task3>
                <!-- '//Guest/Profile/Address/text()' опять же почему-то не работает -->
                <xsl:for-each select="*/*/*/*[1][substring(text(), 1, 12) != 'Pushkinskaya']">
                    <xsl:call-template name="ThirdTask"/>
                </xsl:for-each>
            </task3>
        </result>
    </xsl:template>

    <xsl:template name="FirstTask">
        <name>
            <xsl:value-of select="translate(@Name, 'J', 'j')"/>
        </name>
    </xsl:template>

    <xsl:template name="SecondTask">
        <address>
            <xsl:value-of select="."/>
        </address>
    </xsl:template>

    <xsl:template name="ThirdTask"> 
        <xsl:element name="Address">
            <xsl:attribute name="Name">
                <xsl:value-of select="../../@Name"/>
            </xsl:attribute>
            <xsl:attribute name="Nationalty">
                <xsl:value-of select="../../@Nationalty"/>                
            </xsl:attribute>
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

</xsl:stylesheet>
