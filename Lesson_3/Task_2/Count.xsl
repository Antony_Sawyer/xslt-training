<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    <xsl:key name="Names" match="* | @*" use="local-name()"/>
    <xsl:template match="/">
        <xsl:apply-templates select="//*[generate-id(.) = generate-id(key('Names', local-name()))] | //@*[generate-id(.) = generate-id(key('Names', local-name()))]"/>
    </xsl:template>
    <xsl:template match="* | @*">
        <xsl:value-of select="concat('Node &quot;', local-name(.), '&quot; found ', count(key('Names', local-name())), ' times.', '&#xa;')" />
    </xsl:template>    
</xsl:stylesheet>