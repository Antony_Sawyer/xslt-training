<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    <xsl:key name="AZIndex" match="/*/*" use="substring(@*, 1, 1)"/>
<!-- @* - replace to target attribute, @Name - for example (doesn't matter, if XML have just one attribute)   
   /*/* - path to target element, /*/item - for example (doesn't matter, if the root element contains only one level and one type of child elements   -->
    <xsl:template match="/*">
        <xsl:element name="{local-name()}">
            <xsl:apply-templates
                select="*[generate-id(.) = generate-id(key('AZIndex', substring(@*, 1, 1)))]">
                <xsl:sort select="@*"/>
            </xsl:apply-templates>
        </xsl:element>
    </xsl:template>

    <xsl:template match="*">
        <capital value="{substring(@*, 1, 1)}">
            <xsl:for-each select="key('AZIndex', substring(@*, 1, 1))">
                <xsl:sort select="@*"/>
                <Name>                
                    <xsl:value-of select="@*"/>
                </Name>
            </xsl:for-each>
        </capital>
    </xsl:template>
</xsl:stylesheet>