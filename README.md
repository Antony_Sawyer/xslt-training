## 6. Final task

> *От 29.08 на 03.09*

|Link|Task
|--|--|
[Download](https://bitbucket.org/Antony_Sawyer/xslt-training/raw/ab8c0c4233e6a25d8750c5d02b6d000c8ca64d05/Final_task/Krutenok.zip)|...|

## 4. MySQL

> *От 24.08 на 27.08 до 18:00*

|Link|Task
|--|--|
[Download](https://bitbucket.org/Antony_Sawyer/xslt-training/raw/67cc9fe1fd2caaa2c8cf4c1fdd0371fc241b15cf/Lesson_4/KRUTENOK_VAR_2_GPT_hw4.zip)|...|

## 3. XSLT

> *От 21.08 на 23.08 до 18:00*

|Link|Task
|--|--|
|[Задание №1](https://bitbucket.org/Antony_Sawyer/xslt-training/src/master/Lesson_3/Task_1/)|Данные – имена или фамилии на русском языке.|
|[done]|~~Написать универсальный шаблон для формирования алфавитного указателя из несортированных данных.~~|
|[Задание №2](https://bitbucket.org/Antony_Sawyer/xslt-training/src/master/Lesson_3/Task_2/)|Результат в виде: "Node 'foo' found 5 times." |
|[done]|~~Сосчитать количество элементов и атрибутов документа с различными именами.~~|

## 2. XSLT

> *От 16.08 на 20.08 до 18:00*

|Link|Task
|--|--|
|[Задание №1](https://bitbucket.org/Antony_Sawyer/xslt-training/src/master/Lesson_2/Task_1/)|Входной файл: House.xml|
|[done]|~~Сделать преобразования (используя xsl:apply-templates, xsl:sort и др...) для получения структуры.~~|
|[Задание №2](https://bitbucket.org/Antony_Sawyer/xslt-training/src/master/Lesson_2/Task_2/)|Входной файл - Guests.xml |
|[done]|~~см. №1.3 namespace~~|
|[Задание №3](https://bitbucket.org/Antony_Sawyer/xslt-training/src/master/Lesson_2/Task_3/)|Входной файл - Guests.xml Результат такая же структура НО: |
|[done]|~~**1)** Все элементы Guests и Guest д быть переименованны в Peoples и People соотвественно.~~|
|[done]|~~**2)** Все аттрибуты должны быть в апперкейсе, не значения а имена! Age => AGE.~~|
|[done]|~~**3)** Все текстовые ноды должны быть обернуты в элемент <Text>~~|

## 1. XML & XML Schema Definition

> *От 14.08 на 16.08 до 14:00*

|Link|Task
|--|--|
|[Задание №1](https://bitbucket.org/Antony_Sawyer/xslt-training/src/master/Lesson_1/Task_1/)  | Результат выполнения - два файла: 1 -   xml, второй - xsd.|
|[done]|~~Создать XML, описывающий картинку~~|
|[done]|~~Создать XSD, описывающий данный XML~~|
|[Задание №2](https://bitbucket.org/Antony_Sawyer/xslt-training/src/master/Lesson_1/Task_2/)|Входной файл - Guests.xml|
|[done]|~~**A)** Вывести все имена до "Jimmy" в нижнем регистре.~~|
|[done]|~~**B)** Вывести все адреса гостей после Jimmy которые не белорусы.~~|
|[done]|~~**C)** Вывести все элементы Address у которых в адресе нет подстроки 'Pushkinskaya' и добавить атрибут Name и Nationalty~~|
|[Задание №3](https://bitbucket.org/Antony_Sawyer/xslt-training/src/master/Lesson_1/Task_3/)| Входной файл - Guests.xml 
|[done]|~~**A)** Преобразовать данный документ в текстовую строку.~~|
|[done]|~~**B)** Восстановить исходную структуру из полученной текстовой строки.~~|
